package Composition;
//main entity
public class Shop {
    //composition
    Salesman s1=new Salesman();
    void purchaseProduct()
    {
        s1.provideService();
    }
}
